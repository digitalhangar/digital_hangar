const chai = require('chai');
const User = require('../src/api/models/userModel');

const should = chai.should();

const user = new User({
  first_name: 'Test',
  last_name: 'User',
  email: 'testuser@email.com',
  rating: 4,
  phone_number: '333-333-3333',
});

describe('user model validation', () => {
  describe('valid case', () => {
    it('should create a user given valid data', () => {
      const err = user.validateSync();
      should.not.exist(err);
    });
  });

  describe('invalid cases', () => {
    it('should fail without first name', () => {
      user.first_name = '';
      const err = user.validateSync();
      (err.errors.first_name.kind).should.equal('required');
    });

    it('should fail without last name', () => {
      user.last_name = '';
      const err = user.validateSync();
      (err.errors.last_name.kind).should.equal('required');
    });

    it('should fail without email', () => {
      user.email = '';
      const err = user.validateSync();
      (err.errors.email.kind).should.equal('required');
    });

    it('should fail with rating greater than 5', () => {
      user.rating = 6;
      const err = user.validateSync();
      (err.errors.rating.kind).should.equal('max');
    });

    it('should fail with rating less than 0', () => {
      user.rating = -1;
      const err = user.validateSync();
      (err.errors.rating.kind).should.equal('min');
    });

    it('should fail without phone number', () => {
      user.phone_number = '';
      const err = user.validateSync();
      (err.errors.phone_number.kind).should.equal('required');
    });

    it('should fail without valid phone number', () => {
      user.phone_number = '333-333-333';
      const err = user.validateSync();
      (err.errors.phone_number.kind).should.equal('user defined');
    });
  });
});
