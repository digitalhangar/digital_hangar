const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const User = require('./api/models/userModel.js');
var routes = require('./api/routes/index');
const userRouter = require('./api/routes/user_routes');
const pass = require('./api/controllers/auth.controller');

//use dotenv to securely store Environment variables
// NOTE: Do NOT push .env file to bitbucket
dotenv.config({
  silent: true,
});

const app = express();

mongoose.connect(`mongodb+srv://${process.env.USERNAME}:${process.env.PASSWORD}@cluster0-f2bxv.mongodb.net/test?retryWrites=true`, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

// logger
app.use(logger('dev'));

// bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

// serve static files from public
app.use(express.static(path.join(__dirname, 'client/build')));
// passport
app.use(passport.initialize());
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use('/', routes);
passport.use(pass);
app.use('/users', passport.authenticate('jwt', { session: false }), userRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    message: err.message,
    error: app.get('env') === 'development' ? err : {},
  });
  next();
});

module.exports = app;