var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var User = require('../models/userModel');

const router = express.Router();

/**
 * GET Home page.
 */

router.get('/', (req, res) => {
  res.json('index');
});

router.get('/register', (req, res) => {
  res.json('Registration Endpoint');
});

router.post('/register', (req, res, next) => {
  User.register(new User(req.body), req.body.password, (err) => {
    if (err) {
      return next(err);
    }
    return res.redirect('/login');
  });
});

router.get('/login', (req, res) => {
  res.json('Log In Endpoint');
});

router.post('/login', (req, res) => {
  passport.authenticate('local', { session: false }, (error, user) => {
    if (error || !user) {
      if (!user) {
        res.status(400).send('Email or password is incorrect');
      }
      res.status(400).json({ error });
    }

    req.login(user, { session: false }, (err) => {
      if (error) {
        res.status(400).send({ err });
      }
      const token = jwt.sign({ id: user.id }, 'top_secret');

      res.status(200).send({ user: user.email, token });
    });
  })(req, res);
});


module.exports = router;
