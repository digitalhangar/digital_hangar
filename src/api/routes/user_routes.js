var express = require('express');
var User = require('../models/userModel');

const userRouter = express.Router();

userRouter.use('/profile', (req, res, next) => {
  User.findById(req.user.id, (err, user) => {
    if (err) {
      res.status(500).json(err);
    } else {
      req.user = user;
      next();
    }
  });
});

userRouter.get('/', (req, res) => {
  User.find({}, (err, users) => {
    res.json(users);
  });
});

userRouter.route('/profile')
  .get((req, res) => {
    res.json(req.user);
  })
  .delete((req, res) => {
    req.user.remove((err) => {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(204).json('removed');
      }
    });
  });

module.exports = userRouter;
