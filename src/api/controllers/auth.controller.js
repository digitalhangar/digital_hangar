var User  = require('../models/userModel');

const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

const pass = new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'top_secret',
}, async (token, done) => {
  try {
    const user = await User.findById(token.id);
    if (user) {
      done(null, user);
    }
  } catch (error) {
    done(error);
  }
});

module.exports = pass;
