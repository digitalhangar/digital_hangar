const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

const Schema = mongoose.Schema;

const User = new Schema({
  first_name: {
    type: String,
    required: true,
  },
  last_name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  /* password: {
    type: String,
    required: true,
  },
  */
  rating: {
    type: Number,
    min: 0,
    max: 5,
  },
  phone_number: {
    type: String,
    validate: {
      validator(f) {
        return /\d{3}-\d{3}-\d{4}/.test(f);
      },
      message: props => `${props.value} is not a valid phone number!`,
    },
    required: [true, 'Phone Number is required'],
  },
});

User.plugin(passportLocalMongoose, {
  usernameField: 'email',
});

module.exports = mongoose.model('User', User);
