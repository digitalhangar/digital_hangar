import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { get } from 'http';
import Typography from '@material-ui/core/Typography';
import Card from "./Card/Card.jsx";
import CardHeader from "./Card/CardHeader.jsx";


export default class UserRegistrationForm extends React.Component {
  
  
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmed_password: ""
  };

  change = e => {
    this.props.onChange({ [e.target.name]: e.target.value });
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    // this.props.onSubmit(this.state);

    const { fn, ln, em, pass, confirm_pass } = this.state;
    var formData = new FormData();
    formData.append("firstname", fn)
    formData.append("lastname", ln)
    formData.append("email", em)
    formData.append("password", pass)

    if(pass != confirm_pass)
    {
      console.log("bad");
      return;
    }


    console.log(fn);
    fetch('/api/form-submit-url', {
      method: 'POST',
      body: formData,
    });

    
  };


  render() {
    return (
      <div>
        <form>
          <Card>

            <CardHeader color="primary">
              <Typography variant='display1' align='center' gutterBottom>
                New User Registration
              </Typography>
            </CardHeader>
            
            <TextField
              required
              id="outlined-required-fn"
              name="fn"
              label="First Name"
              margin="normal"
              onChange={e => this.change(e)}
              variant="outlined"
            />
            <br />
            <TextField
              required
              id="outlined-required-ln"
              label="Last Name"
              name="ln"
              margin="normal"
              onChange={e => this.change(e)}
              variant="outlined"
            />
            <br />
            <TextField
              id="outlined-email-input"
              label="Email"
              type="email"
              name="em"
              margin="normal"
              variant="outlined"
              onChange={e => this.change(e)}
              required
            />
            <br />
            <TextField
              id="outlined-password-input"
              label="Password"
              name="pass"
              type="password"
              autoComplete="current-password"
              margin="normal"
              variant="outlined"
              onChange={e => this.change(e)}
              required
            />
            <br />
            <TextField
              id="outlined-confirm-password-input"
              label="Confirm Password"
              name="confirm_pass"
              type="password"
              margin="normal"
              variant="outlined"
              onChange={e => this.change(e)}
              required
            />
            <br />
            <Button variant="contained" color="primary" onClick={e => this.onSubmit(e)}>
              Submit
            </Button>
            
          </Card>
        </form>
      </div>
    );
  }
}