import React from 'react';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';


export default props =>
<AppBar position="static">
        <Toolbar>
          <Typography variant="headline" color="inherit">
             Digital Hangar
          </Typography>
          <Button color="inherit">Login</Button>
          <Button color="inherit">Register</Button>
        </Toolbar>
      </AppBar>