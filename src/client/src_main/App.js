import React, { Component, Fragment } from 'react';
import { Header, Footer } from './Components/Layouts';
import HangarHome from './Components/HangarHome';

export default class app extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <HangarHome />
        <Footer />
      </Fragment>
    );
  }
}
